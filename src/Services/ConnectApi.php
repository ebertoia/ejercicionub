<?php
/**
 * Created by PhpStorm.
 * User: ebertoia
 * Date: 12/11/20
 * Time: 18:49
 */

namespace App\Services;
use Symfony\Contracts\HttpClient\HttpClientInterface;


class ConnectApi
{
    private $client;

    public function __construct(HttpClientInterface $client)
    {
        $this->client = $client;
    }
    public function  getShippingOptions(): array
    {
        $response = $this->client->request(
            'GET',
            'https://shipping-options-api.herokuapp.com/v1/shipping_options'
        );
        $options = $response->getContent();
        $options = json_decode($options, true);
        return $options['shipping_options'];
    }
}