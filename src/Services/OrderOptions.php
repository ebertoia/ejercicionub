<?php
/**
 * Created by PhpStorm.
 * User: ebertoia
 * Date: 12/11/20
 * Time: 19:02
 */

namespace App\Services;


class OrderOptions
{
    private $options;
    public function __construct(array $options)
  {
      $this->options = $options;
  }

  public function costAndDays()
  {

      $args = func_get_args();
      $data = array_shift($args);
      foreach ($args as $n => $field) {
          if (is_string($field)) {
              $tmp = array();
              foreach ($data as $key => $row)
                  $tmp[$key] = $row[$field];
              $args[$n] = $tmp;
          }
      }
      $args[] = &$data;
      call_user_func_array('array_multisort', $args);
      return array_pop($args);

  }

  public function execute(): array
  {
      $sorted = $this->costAndDays($this->options, 'cost', SORT_ASC, 'estimated_days', SORT_ASC);
      return  $sorted;
  }



}