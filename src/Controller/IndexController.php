<?php
/**
 * Created by PhpStorm.
 * User: ebertoia
 */

namespace App\Controller;


use phpDocumentor\Reflection\Types\Object_;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use App\Services\ConnectApi;
use App\Services\OrderOptions;

class IndexController extends AbstractController
{
    /**
     * @Route("/ejercicio", name="ejercicio", options={"expose"=true}, methods={"GET"})
     */
    public function indexAction(ConnectApi $client)
    {
        $option = $client->getShippingOptions();
        $orderOptions = new OrderOptions($option);
        return new JsonResponse($orderOptions->execute());
    }

}