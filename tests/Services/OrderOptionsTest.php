<?php
/**
 * Created by PhpStorm.
 * User: ebertoia
 * Date: 12/11/20
 * Time: 19:48
 */

namespace App\Tests\Services;


use App\Services\OrderOptions;
use PHPUnit\Framework\TestCase;

class OrderOptionsTest extends  TestCase
{

    public function testShippingCostsAndEstimatedDeliveryDates()
    {
        $options = json_decode('[{"name":"Option 1","type":"Delivery","cost":10,"estimated_days":3},
{"name":"Option 2","type":"Custom","cost":10,"estimated_days":3},
{"name":"Option 3","type":"Pickup","cost":10,"estimated_days":3}]',true);

        $orederOptions = new OrderOptions($options);

        $this->assertEquals($options,$orederOptions->execute());

    }

    public function testShippingCostsDifferentEstimatedDeliveryDates()
    {
        $options = json_decode('[{"name":"Option 1","type":"Delivery","cost":10,"estimated_days":5},
{"name":"Option 2","type":"Custom","cost":10,"estimated_days":2},
{"name":"Option 3","type":"Pickup","cost":10,"estimated_days":3}]',true);

        $orderOk = json_decode('[{"name":"Option 2","type":"Custom","cost":10,"estimated_days":2},
{"name":"Option 3","type":"Pickup","cost":10,"estimated_days":3},
{"name":"Option 1","type":"Delivery","cost":10,"estimated_days":5}]', true);

        $orederOptions = new OrderOptions($options);

        $this->assertEquals($orderOk,$orederOptions->execute());

    }


}