FROM php:7.4-cli

RUN apt-get update && apt-get install -y wget vim unzip libicu-dev supervisor gnupg libpng-dev git

RUN cd /tmp/
RUN wget --output-document=/usr/local/bin/composer https://getcomposer.org/composer.phar
RUN chmod a+x /usr/local/bin/composer

RUN wget https://get.symfony.com/cli/installer -O - | bash
RUN mv /root/.symfony/bin/symfony /usr/local/bin/symfony

COPY . /usr/src/app

WORKDIR /usr/src/app

EXPOSE 8000